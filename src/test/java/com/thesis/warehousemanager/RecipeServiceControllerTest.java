package com.thesis.warehousemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.thesis.warehousemanager.model.Ingredient;
import com.thesis.warehousemanager.model.Recipe;

public class RecipeServiceControllerTest extends AbstractTest{

	private String uri = "/recipes";
	
	@Override
	@Before
	public void setup() {
		super.setup();
	}
	
	@Test
	public void getRecipeList() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		String content = mvcResult.getResponse().getContentAsString();
		Recipe[] recipeList = super.mapFromJson(content, Recipe[].class);
		assertEquals(200, status);	
		assertNotNull(recipeList);
		assertTrue(recipeList.length > 0);
	}
	
	@Test
	public void getRecipeById() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/1")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();		
		String content = mvcResult.getResponse().getContentAsString();
		Recipe recipe = super.mapFromJson(content, Recipe.class);
		assertEquals(200, status);
		assertNotNull(recipe);
		assertTrue(recipe.getId() == 1);
	}	
	
	@Test
	public void getRecipeByName() throws Exception{
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/?name=a")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		String content = mvcResult.getResponse().getContentAsString();
		Recipe[] recipeList = super.mapFromJson(content, Recipe[].class);
		assertEquals(200, status);
		assertNotNull(recipeList);
		assertTrue(recipeList[0].getName().contains("a"));
	}
	
	
	@Test
	public void createRecipe() throws Exception {
		// Setup
		Recipe recipe = new Recipe();
		recipe.setName("testingredient1");
		recipe.setActive(true);
		
		String inputJson = super.mapToJson(recipe);
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		
		// Assert
		Recipe createdRecipe = super.mapFromJson(mvcResult.getResponse().getContentAsString(),Recipe.class);
		int status = mvcResult.getResponse().getStatus();
		
		assertEquals(200, status);
		assertNotNull(createdRecipe);
		assertEquals(recipe.getName(), createdRecipe.getName());
		assertEquals(recipe.isActive(), createdRecipe.isActive());
	}
	
	@Test
	public void updateRecipeWithIngredients() throws Exception {
		// Setup
		Recipe recipe = new Recipe();
		recipe.setIngredients(selectIngredients());
		recipe.setCost(recipe.getIngredients().stream().mapToInt(Ingredient::getCost).sum());
		
		String inputJson = super.mapToJson(recipe);
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri+"/1")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

		// Assert
		int status = mvcResult.getResponse().getStatus();
		Recipe updatedRecipe = super.mapFromJson(mvcResult.getResponse().getContentAsString(),Recipe.class);
		assertEquals(200, status);
		assertNotNull(updatedRecipe);		
		assertEquals(recipe.getIngredients(), updatedRecipe.getIngredients());
		assertEquals(recipe.getCost(), updatedRecipe.getCost());		
	}

	private Set<Ingredient> selectIngredients() throws Exception{
		Set<Ingredient> ingredients = new HashSet<>();
		for(int i = 1; i < 3 ; i++) {
			String ingredientResult = mvc.perform(MockMvcRequestBuilders.get("/ingredients/"+i)
					.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn().getResponse().getContentAsString();
			Ingredient testIngredient = super.mapFromJson(ingredientResult, Ingredient.class); 
			ingredients.add(testIngredient);
		}
		return ingredients;
	}
	
}
