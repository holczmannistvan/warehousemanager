package com.thesis.warehousemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.thesis.warehousemanager.model.Supplier;

public class SupplierServiceControllerTest extends AbstractTest{
	
	String uri = "/suppliers";
	
	@Override
	@Before
	public void setup() {
		super.setup();		
	}
	
	@Test
	public void getSupplierList() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();		
		Supplier[] supplierList = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Supplier[].class);
		
		assertEquals(200, status);
		assertNotNull(supplierList);
		assertTrue(supplierList.length > 0);
	}
	
	@Test
	public void getSupplierById() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/1")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		Supplier supplier = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Supplier.class);
		
		assertEquals(200, status);
		assertNotNull(supplier);
		assertTrue(supplier.getId() == 1);
	}
		
	@Test
	public void getSupplierByName() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/?name=a")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		Supplier[] supplierList = super.mapFromJson( mvcResult.getResponse().getContentAsString(), Supplier[].class);
		
		assertEquals(200, status);
		assertNotNull(supplierList);
		assertTrue(supplierList[0].getName().contains("a"));
	}
	
	@Test
	public void createSupplier() throws Exception {
		// Setup
		Supplier supplier = new Supplier();
		supplier.setName("testSupplier");
		supplier.setAccountNumber("12345678");
		supplier.setBillingAddress("Budapest, Fogocska utca 3 1/5");
		supplier.setActive(true);
		
		String inputJson = super.mapToJson(supplier);
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		
		// Assert
		Supplier createdSupplier = super.mapFromJson(mvcResult.getResponse().getContentAsString(),Supplier.class);
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		assertNotNull(createdSupplier);
		assertEquals(supplier.getName(), createdSupplier.getName());
		assertEquals(supplier.getAccountNumber(), createdSupplier.getAccountNumber());
		assertEquals(supplier.getBillingAddress(), createdSupplier.getBillingAddress());
		assertEquals(supplier.isActive(), createdSupplier.isActive());
	}
	
	@Test
	public void updateSupplier() throws Exception {
		// Setup
		Supplier supplier = new Supplier();
		supplier.setName("changedSupplier");
		supplier.setAccountNumber("98765");
		supplier.setBillingAddress("BP Bujocska utca 1 3/6");
		supplier.setActive(false);
		
		String inputJson = super.mapToJson(supplier);
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri+"/1")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();	
		String content = mvcResult.getResponse().getContentAsString();
		Supplier updatedSupplier = super.mapFromJson(content, Supplier.class);		
		
		assertEquals(200, status);
		assertNotNull(updatedSupplier);
		assertEquals(supplier.getName(), updatedSupplier.getName());
		assertEquals(supplier.getAccountNumber(), updatedSupplier.getAccountNumber());
		assertEquals(supplier.getBillingAddress(), updatedSupplier.getBillingAddress());
		assertEquals(supplier.isActive(), updatedSupplier.isActive());
	}	
}












