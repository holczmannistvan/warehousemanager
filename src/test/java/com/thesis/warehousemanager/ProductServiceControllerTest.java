package com.thesis.warehousemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.thesis.warehousemanager.model.Product;

public class ProductServiceControllerTest extends AbstractTest{
	
	private String uri = "/products";
	
	@Override
	@Before
	public void setup() {
		super.setup();
	}
	
	@Test
	public void createProduct() throws Exception {
		// Setup
		Product product = new Product();
		product.setName("testproduct1");
		product.setQuantity(10);
		product.setPrice(10);
		product.setActive(true);

		String inputJson = super.mapToJson(product);

		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		Product createdProduct = super.mapFromJson(mvcResult.getResponse().getContentAsString(),Product.class);
		
		assertNotNull(createdProduct);
		assertEquals(200, status);
		assertEquals(product.getName(), createdProduct.getName());
		assertEquals(product.getQuantity(), createdProduct.getQuantity());
		assertEquals(product.getPrice(), createdProduct.getPrice());
		assertEquals(product.isActive(), createdProduct.isActive());
	}
	
	@Test
	public void getProductsList() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		//Assert
		int status = mvcResult.getResponse().getStatus();
		Product[] productList = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Product[].class);
		
		assertEquals(200, status);
		assertNotNull(productList);
		assertTrue(productList.length > 0);
	}
	
	@Test
	public void getProductByName() throws Exception{
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/?name=a")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		Product[] productList = super.mapFromJson( mvcResult.getResponse().getContentAsString(), Product[].class);
		
		assertEquals(200, status);
		assertNotNull(productList);
		assertTrue(productList[0].getName().contains("a"));
	}
	
	@Test
	public void updateProduct() throws Exception {
		// Setup
		Product product = new Product();
		product.setName("updatedproduct");
		product.setQuantity(11);
		product.setPrice(11);
		product.setActive(false);
		
		String inputJson = super.mapToJson(product);

		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri+"/1")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

		// Assert
		int status = mvcResult.getResponse().getStatus();
		String content = mvcResult.getResponse().getContentAsString();
		Product updatedProduct = super.mapFromJson(content, Product.class);
		
		assertEquals(200, status);
		assertNotNull(updatedProduct);
		assertEquals(product.getName(), updatedProduct.getName());
		assertEquals(product.getQuantity(), updatedProduct.getQuantity());
		assertEquals(product.getPrice(), updatedProduct.getPrice());
		assertEquals(product.isActive(), updatedProduct.isActive());		
	}
}
