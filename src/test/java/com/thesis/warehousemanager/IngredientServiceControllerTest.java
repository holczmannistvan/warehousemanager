package com.thesis.warehousemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.thesis.warehousemanager.model.Ingredient;
import com.thesis.warehousemanager.model.Supplier;

public class IngredientServiceControllerTest extends AbstractTest{
	
	private String uri = "/ingredients";
	
	@Override
	@Before
	public void setup() {
		super.setup();
	}
	
	@Test
	public void getIngredientsList() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		//Assert
		int status = mvcResult.getResponse().getStatus();		
		String content = mvcResult.getResponse().getContentAsString();
		Ingredient[] ingredientList = super.mapFromJson(content, Ingredient[].class);
		
		assertEquals(200, status);
		assertNotNull(ingredientList);
		assertTrue(ingredientList.length > 0);
	}
	
	@Test
	public void getIngredientById() throws Exception{
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/1")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		Ingredient ingredient = super.mapFromJson(mvcResult.getResponse()
				.getContentAsString(), Ingredient.class);
		
		assertEquals(200, status);
		assertNotNull(ingredient);
		assertTrue(ingredient.getId() == 1);
	}
	
	@Test
	public void getIngredientByName() throws Exception{
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/?name=a")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		Ingredient[] ingredientList = super.mapFromJson( mvcResult.getResponse()
				.getContentAsString(), Ingredient[].class);
		
		assertEquals(200, status);
		assertNotNull(ingredientList);
		assertTrue(ingredientList[0].getName().contains("a"));
	}
	
	@Test
	public void createIngredient() throws Exception {
		// Setup
		Ingredient ingredient = new Ingredient();
		ingredient.setName("testingredient1");
		ingredient.setCost(10);
		ingredient.setQuantity(100);
		ingredient.setActive(true);
		ingredient.setSupplier(selectSupplier());
		
		String inputJson = super.mapToJson(ingredient);
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		
		// Assert
		Ingredient createdIngredient = super.mapFromJson(mvcResult.getResponse().getContentAsString(),Ingredient.class);
		int status = mvcResult.getResponse().getStatus();
		
		assertEquals(200, status);
		assertNotNull(createdIngredient);
		assertEquals(ingredient.getName(), createdIngredient.getName());
		assertEquals(ingredient.getCost(), createdIngredient.getCost());
		assertEquals(ingredient.isActive(), createdIngredient.isActive());
		assertEquals(ingredient.getSupplier().getName(), createdIngredient.getSupplier().getName());
	}

	private Supplier selectSupplier() throws Exception{
		String supplierResult = mvc.perform(MockMvcRequestBuilders.get("/suppliers/1")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn().getResponse().getContentAsString();
		Supplier testSupplier = super.mapFromJson(supplierResult, Supplier.class); 
		return testSupplier;
	}
	
	
	@Test
	public void updateIngredient() throws Exception {
		// Setup
		Ingredient ingredient = new Ingredient();
		ingredient.setName("ingredientUpdated");
		ingredient.setCost(101);
		ingredient.setActive(false);
		
		//Test
		String inputJson = super.mapToJson(ingredient);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri+"/1")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

		// Assert
		int status = mvcResult.getResponse().getStatus();
		Ingredient updatedIngredient = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Ingredient.class);		
		
		assertEquals(200, status);
		assertNotNull(updatedIngredient);
		assertEquals(ingredient.getName(), updatedIngredient.getName());
		assertEquals(ingredient.getCost(), updatedIngredient.getCost());
		assertEquals(ingredient.isActive(), updatedIngredient.isActive());
	}	
}
