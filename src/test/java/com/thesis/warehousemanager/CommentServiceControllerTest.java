package com.thesis.warehousemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.thesis.warehousemanager.model.Comment;
import com.thesis.warehousemanager.model.Recipe;

public class CommentServiceControllerTest extends AbstractTest{
	
	String uri = "/comments";
	
	@Override
	@Before
	public void setup() {
		super.setup();		
	}
	
	@Test
	public void getCommentList() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();		
		Comment[] commentList = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Comment[].class);
		
		assertEquals(200, status);
		assertNotNull(commentList);
		assertTrue(commentList.length > 0);
	}
	
	@Test
	public void getCommentById() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/1")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		Comment comment = super.mapFromJson(mvcResult.getResponse().getContentAsString(), Comment.class);
		
		assertEquals(200, status);
		assertNotNull(comment);
		assertTrue(comment.getId() == 1);
	}
	
	@Test
	public void createComment() throws Exception {
		// Setup
		Comment comment = new Comment();
		comment.setComment("testComment written here");
		comment.setRecipe(selectRecipe());
		
		String inputJson = super.mapToJson(comment);
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		
		// Assert
		Comment createdComment = super.mapFromJson(mvcResult.getResponse().getContentAsString(),Comment.class);
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
		assertNotNull(createdComment);
		assertEquals(comment.getComment(), createdComment.getComment());
		assertEquals(comment.getRecipe().getName(), createdComment.getRecipe().getName());
	}
	

	private Recipe selectRecipe() throws Exception {
		String recipeResult = mvc.perform(MockMvcRequestBuilders.get("/recipes/1")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn().getResponse().getContentAsString();
		Recipe testRecipe = super.mapFromJson(recipeResult, Recipe.class); 
		return testRecipe;
	}
	
	@Test
	public void updateComment() throws Exception {
		// Setup
		Comment comment = new Comment();
		comment.setComment("changedComment written here");
		
		String inputJson = super.mapToJson(comment);
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri+"/1")
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();	
		String content = mvcResult.getResponse().getContentAsString();
		Comment updatedComment = super.mapFromJson(content, Comment.class);		
		
		assertEquals(200, status);
		assertNotNull(updatedComment);
		assertEquals(comment.getComment(), updatedComment.getComment());
	}	
}












