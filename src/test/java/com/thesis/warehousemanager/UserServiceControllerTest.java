package com.thesis.warehousemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.thesis.warehousemanager.model.User;

public class UserServiceControllerTest extends AbstractTest{
	
	private String uri = "/users";
	
	@Override
	@Before
	public void setup() {
		super.setup();
	}
	
	@Test
	public void getUsersList() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		//Assert
		int status = mvcResult.getResponse().getStatus();		
		String content = mvcResult.getResponse().getContentAsString();
		User[] userList = super.mapFromJson(content, User[].class);
		
		assertEquals(200, status);
		assertNotNull(userList);
		assertTrue(userList.length > 0);
	}	
	
	@Test
	public void getUsernamesList() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri +"/usernames")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		//Assert
		int status = mvcResult.getResponse().getStatus();		
		String content = mvcResult.getResponse().getContentAsString();
		String[] userList = super.mapFromJson(content, String[].class);
		
		assertEquals(200, status);
		assertNotNull(userList);
		assertTrue(userList.length > 0);
	}	
	
	@Test
	public void getUserById() throws Exception{
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/1")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		User user = super.mapFromJson(mvcResult.getResponse().getContentAsString(), User.class);
		
		assertEquals(200, status);
		assertNotNull(user);
		assertTrue(user.getId() == 1);
	}
	
	
	@Test
	public void getUserByUsername() throws Exception{
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/byUsername/?username=admin")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		User user = super.mapFromJson( mvcResult.getResponse().getContentAsString(), User.class);
		
		assertEquals(200, status);
		assertNotNull(user);
		assertEquals(user.getUsername(), "admin");
	}
	
	@Test
	public void getUserByUsernameAndPassword() throws Exception{
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri+"/byUsernameAndPass/?username=admin&password=admin")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		User user = super.mapFromJson( mvcResult.getResponse().getContentAsString(), User.class);
		
		assertEquals(200, status);
		assertNotNull(user);
		assertEquals(user.getUsername(), "admin" );
		assertEquals(user.getPassword(), "admin" );
	}
	
	@Test
	public void createUser() throws Exception {
		// Setup
		User user = new User();
		user.setUsername("testAdmin");
		user.setPassword("testAdmin");
		user.setRole("ADMIN");
		
		String inputJson = super.mapToJson(user);
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		
		// Assert
		User createdUser = super.mapFromJson(mvcResult.getResponse().getContentAsString(),User.class);
		int status = mvcResult.getResponse().getStatus();
		
		assertEquals(200, status);
		assertNotNull(createdUser);
		assertEquals(user.getUsername(), createdUser.getUsername());
		assertEquals(user.getPassword(), createdUser.getPassword());
		assertEquals(user.getRole(), createdUser.getRole());
	}
	
	@Test
	public void updateUser() throws Exception {
		// Setup
		User user = new User();
		user.setUsername("userUpdated");
		user.setPassword("updatedPassword");
		user.setRole("USER");
		
		//Test
		String inputJson = super.mapToJson(user);
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri+"/2").contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();

		// Assert
		int status = mvcResult.getResponse().getStatus();
		User updatedUser = super.mapFromJson(mvcResult.getResponse().getContentAsString(), User.class);		
		
		assertEquals(200, status);
		assertNotNull(updatedUser);
		assertEquals(user.getUsername(), updatedUser.getUsername());
		assertEquals(user.getPassword(), updatedUser.getPassword());
		assertEquals(user.getRole(), updatedUser.getRole());
	}	
	
}
