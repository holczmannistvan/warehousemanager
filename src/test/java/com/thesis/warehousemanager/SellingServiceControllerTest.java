package com.thesis.warehousemanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.thesis.warehousemanager.model.Product;
import com.thesis.warehousemanager.model.Selling;

public class SellingServiceControllerTest extends AbstractTest{
	
	private String uri = "/sellings";
	
	@Override
	@Before
	public void setup() {
		super.setup();
	}
	
	@Test
	public void getSellingsList() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		//Assert
		int status = mvcResult.getResponse().getStatus();		
		String content = mvcResult.getResponse().getContentAsString();
		Selling[] sellingList = super.mapFromJson(content, Selling[].class);
		
		assertEquals(200, status);
		assertNotNull(sellingList);
		assertTrue(sellingList.length > 0);
	}
	
	@Test
	public void getSellingById() throws Exception{
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/1")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		Selling selling = super.mapFromJson(mvcResult.getResponse()
				.getContentAsString(), Selling.class);
		
		assertEquals(200, status);
		assertNotNull(selling);
		assertTrue(selling.getId() == 1);
	}
	
	@Test
	public void getSellingsByCustomerGroup() throws Exception{
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri + "/?customerGroup=Wholesaler")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		Selling[] sellingList = super.mapFromJson( mvcResult.getResponse()
				.getContentAsString(), Selling[].class);
		
		assertEquals(200, status);
		assertNotNull(sellingList);
		assertEquals(sellingList[0].getCustomerGroup(), "Wholesaler");
	}
	
	@Test
	public void createSelling() throws Exception {
		// Setup
		Selling selling = new Selling();
		selling.setCustomerGroup("Wholesaler");
		selling.setProduct(selectProduct());
		selling.setQuantity(2);
		selling.setEarning(selling.getQuantity() * selling.getProduct().getPrice());
		
		String inputJson = super.mapToJson(selling);
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
				.contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
		
		// Assert
		Selling createdSelling = super.mapFromJson(mvcResult.getResponse().getContentAsString(),Selling.class);
		int status = mvcResult.getResponse().getStatus();
		
		assertEquals(200, status);
		assertNotNull(createdSelling);
		assertEquals(selling.getCustomerGroup(), createdSelling.getCustomerGroup());
		assertEquals(selling.getProduct(), createdSelling.getProduct());		
		assertEquals(selling.getQuantity(), createdSelling.getQuantity());
		assertEquals(selling.getEarning(), createdSelling.getEarning());
	}

	private Product selectProduct() throws Exception {
		String productResult = mvc.perform(MockMvcRequestBuilders.get("/products/1")
				.accept(MediaType.APPLICATION_JSON_VALUE)).andReturn().getResponse().getContentAsString();
		Product testProduct = super.mapFromJson(productResult, Product.class); 
		return testProduct;
	}
	
	@Test
	public void deleteSelling() throws Exception {
		// Setup
		
		// Test
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri+"/2")).andReturn();
		
		// Assert
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
}
