package com.thesis.warehousemanager.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ingredient")
public class Ingredient implements Serializable{
	  @Id
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  private int id;
	  
	  @Column (name = "name")
	  private String name;
	
	  @Column (name = "cost")
	  private int cost;
	  
	  @Column (name = "quantity")
	  private int quantity;
	  
	  @ManyToOne
	  @JoinColumn(name = "supplier_id")
	  private Supplier supplier;
	 
	  @JsonIgnore
	  @ManyToMany(mappedBy = "ingredients" ,cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	  private Set<Recipe> recipes = new HashSet<>();
	  
	  @Column(name = "active")
	  private boolean active;
		
	  
	  public Ingredient(String name, int cost, int quantity, int brand, Supplier supplier) {
		  this.name = name;
		  this.cost = cost;
		  this.quantity = quantity;
		  this.supplier = supplier;
	  }
}
