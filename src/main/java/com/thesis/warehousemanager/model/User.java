package com.thesis.warehousemanager.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user")
public class User implements Serializable{
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "username")
	private String username;

	@Column (name = "password")
	private String password;
	
	@Column(name = "role")
	private String role;
	
	public enum Role {
		ADMIN("ADMIN"),
		USER("USER");
		
		private String type;
		
		Role(String type){
			this.type = type;
		}
	}
}	
	
	
	
	
	
	