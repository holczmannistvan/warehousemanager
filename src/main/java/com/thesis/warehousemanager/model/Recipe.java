package com.thesis.warehousemanager.model;

import java.io.Serializable;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@SuppressWarnings("serial")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = "ingredients")
@Table(name = "recipe")
public class Recipe implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "cost")
	private int cost;
	  
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinTable(name = "recipe_ingredient",
	joinColumns = @JoinColumn(name = "recipe_id", referencedColumnName = "id"),
	inverseJoinColumns = @JoinColumn(name = "ingredient_id", referencedColumnName = "id"))    
	private Set<Ingredient> ingredients;
	
	@Column(name = "active")
	private boolean active;
	
	
    public Recipe(String name, int cost, Ingredient... ingredients) {
    	this.name = name;
    	this.cost = cost;
        this.ingredients = Stream.of(ingredients).collect(Collectors.toSet());
        this.ingredients.forEach(x -> x.getRecipes().add(this));
    }
}
