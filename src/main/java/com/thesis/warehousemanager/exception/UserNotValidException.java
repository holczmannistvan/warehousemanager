package com.thesis.warehousemanager.exception;

@SuppressWarnings("serial")
public class UserNotValidException extends Exception{
	  public UserNotValidException(String message) {
	    super(message);
	  }
	}

