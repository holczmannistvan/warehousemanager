package com.thesis.warehousemanager.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thesis.warehousemanager.exception.ResourceNotFoundException;
import com.thesis.warehousemanager.model.Selling;
import com.thesis.warehousemanager.repository.SellingRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class SellingController {
	
	@Autowired
	private SellingRepository sellingRepository;

	@GetMapping("/sellings")
	public Collection<Selling> getAllSelling()   {
		return sellingRepository.findAll();
	}
	
	@GetMapping("/sellings/{id}")
	public Selling getSellingById(@PathVariable(value = "id") int sellingId) {
		return sellingRepository.findById(sellingId).orElseThrow(() -> new ResourceNotFoundException("Selling", "id", sellingId));
	}
	
	@GetMapping("/sellings/")
	public Collection<Selling> searchSellingByCustomerGroup(@RequestParam(value = "customerGroup") String sellingName){
		return sellingRepository.findByCustomerGroup(sellingName);
	}
	
	@PostMapping("/sellings")
	public Selling createSelling(@Valid @RequestBody Selling selling) {
		return sellingRepository.save(selling);
	}
	
	@PutMapping("/sellings/{id}")
	public Selling updateSelling(@PathVariable( value = "id") int sellingId, @Valid @RequestBody Selling sellingDetails) {
		Selling selling = sellingRepository.findById(sellingId).orElseThrow(() -> new ResourceNotFoundException("Selling", "id", sellingId));
		
		selling.setCustomerGroup(sellingDetails.getCustomerGroup());
		selling.setEarning(sellingDetails.getEarning());
		selling.setQuantity(sellingDetails.getQuantity()); 
		
		Selling updatedSelling = sellingRepository.save(selling);
		return updatedSelling;
	}
	
	@DeleteMapping("/sellings/{id}")
	public ResponseEntity<?> deleteSelling(@PathVariable( value = "id") int sellingId) {
		Selling selling = sellingRepository.findById(sellingId).orElseThrow(() -> new ResourceNotFoundException("Ingredeient", "id", sellingId));
		
		sellingRepository.delete(selling);
		return ResponseEntity.ok().build();
	}

}
