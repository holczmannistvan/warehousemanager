package com.thesis.warehousemanager.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thesis.warehousemanager.exception.ResourceNotFoundException;
import com.thesis.warehousemanager.model.Ingredient;
import com.thesis.warehousemanager.repository.IngredientRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class IngredientController {
	
	@Autowired
	private IngredientRepository ingredientRepository;

	@GetMapping("/ingredients")
	public Collection<Ingredient> getAllIngredient()   {
		return ingredientRepository.findAll();
	}
	
	@GetMapping("/ingredients/{id}")
	public Ingredient getIngredientById(@PathVariable(value = "id") int ingredientId) {
		return ingredientRepository.findById(ingredientId).orElseThrow(() -> new ResourceNotFoundException("Ingredient", "id", ingredientId));
	}
	
	@GetMapping("/ingredients/")
	public Collection<Ingredient> searchIngredientByName(@RequestParam(value = "name") String ingredientName){
		return ingredientRepository.findByNameContains(ingredientName);
	}
	
	@PostMapping("/ingredients")
	public Ingredient createIngredient(@Valid @RequestBody Ingredient ingredient) {
		return ingredientRepository.save(ingredient);
	}
	
	@PutMapping("/ingredients/{id}")
	public Ingredient updateIngredient(@PathVariable( value = "id") int ingredientId, @Valid @RequestBody Ingredient ingredientDetails) {
		Ingredient ingredient = ingredientRepository.findById(ingredientId).orElseThrow(() -> new ResourceNotFoundException("Ingredient", "id", ingredientId));
		
		ingredient.setName(ingredientDetails.getName());
		ingredient.setCost(ingredientDetails.getCost());
		ingredient.setQuantity(ingredientDetails.getQuantity());
		ingredient.setSupplier(ingredientDetails.getSupplier());
		ingredient.setActive(ingredientDetails.isActive());
		
		
		Ingredient updatedIngredient = ingredientRepository.save(ingredient);
		return updatedIngredient;
	}
	
	@DeleteMapping("/ingredients/{id}")
	public ResponseEntity<?> deleteIngredient(@PathVariable( value = "id") int ingredientId) {
		Ingredient ingredient = ingredientRepository.findById(ingredientId).orElseThrow(() -> new ResourceNotFoundException("Ingredeient", "id", ingredientId));
		
		ingredientRepository.delete(ingredient);
		return ResponseEntity.ok().build();
	}

}
