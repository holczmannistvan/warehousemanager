package com.thesis.warehousemanager.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thesis.warehousemanager.exception.ResourceNotFoundException;
import com.thesis.warehousemanager.model.Supplier;
import com.thesis.warehousemanager.repository.SupplierRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class SupplierController {
	
	@Autowired
	private SupplierRepository supplierRepository;
	
	@GetMapping("/suppliers")
	public Collection<Supplier> getAllSupplier(){
		return supplierRepository.findAll();
	}
	
	@GetMapping("/suppliers/{id}")
	public Supplier getSupplierById(@PathVariable(value = "id") int supplierId ) {
		return supplierRepository.findById(supplierId).orElseThrow(() -> new ResourceNotFoundException("Supplier", "id", supplierId));
	}
	
	@GetMapping("/suppliers/")
	public Collection<Supplier> searchSupplierByName(@RequestParam(value = "name") String suppplierName){
		return supplierRepository.findByNameContains(suppplierName);
	}
	
	@PostMapping("/suppliers")
	public Supplier createSupplier(@Valid @RequestBody Supplier supplier) {
		return supplierRepository.save(supplier);
	}
	
	@PutMapping("/suppliers/{id}")
	public Supplier upgradeSupplier(@PathVariable( value = "id") int supplierId, @Valid @RequestBody Supplier supplierDetails) {
		Supplier supplier = supplierRepository.findById(supplierId).orElseThrow(() -> new ResourceNotFoundException("Supplier", "id", supplierId));
	
		supplier.setName(supplierDetails.getName());
		supplier.setAccountNumber(supplierDetails.getAccountNumber());
		supplier.setBillingAddress(supplierDetails.getBillingAddress());
		supplier.setActive(supplierDetails.isActive());
		
		Supplier upgradedSupplier = supplierRepository.save(supplier);
		return upgradedSupplier;
	}
	
	@DeleteMapping("/suppliers/{id}")
	public ResponseEntity<?> deleteSupplier(@PathVariable( value = "id") int supplierId){
		Supplier supplier = supplierRepository.findById(supplierId).orElseThrow(() -> new ResourceNotFoundException("Ingredeient", "id", supplierId));
		
		supplierRepository.delete(supplier);
		return ResponseEntity.ok().build();
	}
 
}
