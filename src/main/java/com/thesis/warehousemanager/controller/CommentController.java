package com.thesis.warehousemanager.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.thesis.warehousemanager.exception.ResourceNotFoundException;
import com.thesis.warehousemanager.model.Comment;
import com.thesis.warehousemanager.repository.CommentRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class CommentController {
	
	@Autowired
	private CommentRepository commentRepository;

	@GetMapping("/comments")
	public Collection<Comment> getAllComment()   {
		return commentRepository.findAll();
	}
	
	@GetMapping("/comments/{id}")
	public Comment getCommentById(@PathVariable(value = "id") int commentId) {
		return commentRepository.findById(commentId).orElseThrow(() -> new ResourceNotFoundException("Comment", "id", commentId));
	}
	
	@PostMapping("/comments")
	public Comment createComment(@Valid @RequestBody Comment comment) {
		return commentRepository.save(comment);
	}
	
	@PutMapping("/comments/{id}")
	public Comment updateComment(@PathVariable( value = "id") int commentId, @Valid @RequestBody Comment commentDetails) {
		Comment comment = commentRepository.findById(commentId).orElseThrow(() -> new ResourceNotFoundException("Comment", "id", commentId));
		
		comment.setComment(commentDetails.getComment());
		
		Comment upgradedComment = commentRepository.save(comment);
		return upgradedComment;
	}
	
	@DeleteMapping("/comments/{id}")
	public ResponseEntity<?> deleteComment(@PathVariable( value = "id") int commentId) {
		Comment comment = commentRepository.findById(commentId).orElseThrow(() -> new ResourceNotFoundException("Ingredeient", "id", commentId));
		
		commentRepository.delete(comment);
		return ResponseEntity.ok().build();
	}

}
