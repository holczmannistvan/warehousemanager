package com.thesis.warehousemanager.controller;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thesis.warehousemanager.exception.ResourceNotFoundException;
import com.thesis.warehousemanager.exception.UserNotValidException;
import com.thesis.warehousemanager.model.User;
import com.thesis.warehousemanager.repository.UserRepository;


@CrossOrigin(origins = "http://localhost:4200" , allowedHeaders = "*")
@RestController
public class UserController {
	
	@Autowired
	private UserRepository userRepository;
	
	private User user;

	@GetMapping("/users")
	public Collection<User> getAllUser()   {
			return userRepository.findAll();
	}
	
	@GetMapping("/users/usernames")
    public Collection<String> getAllUsername() {
		Collection<User> users = userRepository.findAll(); 
		Collection<String> usernames = users.stream().map(user -> user.getUsername()).collect(Collectors.toList());
        return usernames;
    }
	
	@GetMapping("/users/{id}")
	public User getUserById(@PathVariable(value = "id") int userId) {
		return userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
	}
	
	@GetMapping("/users/byUsername/")
	public User findUserByUsername(@RequestParam(value = "username") String username) throws ResourceNotFoundException {
			return userRepository.findByUsername(username);
	}
	

	@GetMapping("/users/byUsernameAndPass/")
	public User findUserByUserameAndPassword(
			@RequestParam(value = "username") String username, 
			@RequestParam(value = "password") String password) throws ResourceNotFoundException{
			return userRepository.findByUsernameAndPassword(username, password);

	}
	
	@PostMapping("/users")
	public User createUser(@Valid @RequestBody User user) {
		return userRepository.save(user);
	}
	
	@PutMapping("/users/{id}")
	public User updateUser(@PathVariable( value = "id") int userId, @Valid @RequestBody User userDetails) {
		User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User", "id", userId));
		
		user.setUsername(userDetails.getUsername());
		user.setPassword(userDetails.getPassword());
		user.setRole(userDetails.getRole());

		User updatedUser = userRepository.save(user);
		return updatedUser;
	}
	
	@DeleteMapping("/users/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable( value = "id") int userId) {
		User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("Ingredeient", "id", userId));
		
		userRepository.delete(user);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/login")
	public String login(Model model) {
		model.addAttribute(new User());
		return "\"login\"";
	}
	
	@PostMapping("/login")
	public User login(@RequestBody User user, Model model) throws UserNotValidException {
		try {
			if(isValid(user)) {
				return this.user = userRepository.findByUsername(user.getUsername());				
			}
			throw new UserNotValidException("User not valid!");
		}
		catch (UserNotValidException e) {
		model.addAttribute("error", true);
		return null;
		}
	}

	private boolean isValid(User user) {
		User foundUser = userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		return foundUser != null;
	}
	
	@GetMapping("/getCurrentUser")
	public String getCurrentUser() {
	    return user == null ? "Unknown" : user.getUsername();
	}
	
}
