package com.thesis.warehousemanager.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thesis.warehousemanager.exception.ResourceNotFoundException;
import com.thesis.warehousemanager.model.Recipe;
import com.thesis.warehousemanager.repository.RecipeRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class RecipeController {
	
	@Autowired
	private RecipeRepository recipeRepository;
	
	@GetMapping("/recipes")
	public Collection<Recipe> getAllRecipes(){
		return recipeRepository.findAll();
	}
	
	@GetMapping("/recipes/{id}")
	public Recipe getRecipes(@PathVariable( value = "id") int recipeId) {
		return recipeRepository.findById(recipeId).orElseThrow(() -> new ResourceNotFoundException("Recipe", "id", recipeId));
	}	
	
	@GetMapping("/recipes/")
	public Collection<Recipe> searchRecipeByName(@RequestParam(value = "name") String recipeId){
		return recipeRepository.findByNameContains(recipeId);
	}	
	
	@PostMapping("/recipes") 
	public Recipe createRecipe(@Valid @RequestBody Recipe recipe) {		
		
		return recipeRepository.save(recipe);
	}
	
	@PutMapping("/recipes/{id}")
	public Recipe upgradeRecipe (@PathVariable( value = "id") int recipeId, @Valid @RequestBody Recipe recipeDetails) {
		Recipe recipe = recipeRepository.findById(recipeId).orElseThrow(() -> new ResourceNotFoundException("Recipe", "id", recipeId));
		
		recipe.setName(recipeDetails.getName());
		recipe.setCost(recipeDetails.getCost());
		recipe.setIngredients(recipeDetails.getIngredients());
		recipe.setActive(recipeDetails.isActive());
		
		
		Recipe updatedRecipe = recipeRepository.save(recipe);
		return updatedRecipe;
	}
	
	@DeleteMapping("/recipes/{id}")
	public ResponseEntity<?> deleteRecipe(@PathVariable( value = "id") int recipeId) {
		Recipe recipe = recipeRepository.findById(recipeId).orElseThrow(() -> new ResourceNotFoundException("Recipe", "id", recipeId));
		
		recipeRepository.delete(recipe);
		return ResponseEntity.ok().build();
	}
}
