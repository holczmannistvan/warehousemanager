package com.thesis.warehousemanager.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.thesis.warehousemanager.exception.ResourceNotFoundException;
import com.thesis.warehousemanager.model.Product;
import com.thesis.warehousemanager.repository.ProductRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ProductController {
	
	@Autowired
	private ProductRepository productRepository;
	
	@GetMapping("/products")
	public Collection<Product> getAllProduct() {
		return productRepository.findAll();
	}
	
	@GetMapping("/products/{id}")
	public Product getProductById(@PathVariable(value = "id") int productId) {
		return productRepository.findById(productId).orElseThrow(() -> new ResourceNotFoundException("Product", "id", productId));
	}
	
	@GetMapping("/products/")
	public Collection<Product> searchProductByName(@RequestParam(value = "name") String productName){
		return productRepository.findByNameContains(productName);
	}
	
	
	@PostMapping("/products")
	public Product createProduct(@Valid @RequestBody Product product) {
		return productRepository.save(product);
	}
	
	
	@PutMapping("/products/{id}")
	public Product updateProduct(@PathVariable( value = "id") int productId, @Valid @RequestBody Product productDetail) {
		Product product = productRepository.findById(productId).orElseThrow(() -> new ResourceNotFoundException("Product", "id", productId));
		
		product.setName(productDetail.getName());
		product.setPrice(productDetail.getPrice());
		product.setQuantity(productDetail.getQuantity());
		product.setRecipe(productDetail.getRecipe());
		product.setActive(productDetail.isActive());
		
		Product updatedProduct = productRepository.save(product);
		return updatedProduct;
	}
	
	@DeleteMapping("/products/{id}")
	public ResponseEntity<?> deleteProduct(@PathVariable( value = "id") int productId) {
		Product product = productRepository.findById(productId).orElseThrow(() -> new ResourceNotFoundException("Product", "id", productId));
		
		productRepository.delete(product);
		return ResponseEntity.ok().build();
	}
	
}
