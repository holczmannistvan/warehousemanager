package com.thesis.warehousemanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thesis.warehousemanager.model.Comment;

@Transactional
@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer>{
} 
