package com.thesis.warehousemanager.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thesis.warehousemanager.model.Recipe;

@Transactional
@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Integer> {
	Recipe findByName(String name);
	Collection<Recipe> findByNameContains(String name);
}
