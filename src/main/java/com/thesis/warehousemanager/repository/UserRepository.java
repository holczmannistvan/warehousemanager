package com.thesis.warehousemanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thesis.warehousemanager.model.User;

@Transactional
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	User findByUsername(String Username);
	User findByUsernameAndPassword(String username, String password);
}
