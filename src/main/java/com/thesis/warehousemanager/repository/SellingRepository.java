package com.thesis.warehousemanager.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thesis.warehousemanager.model.Selling;

@Transactional
@Repository
public interface SellingRepository extends JpaRepository<Selling, Integer>{
	Collection<Selling> findByCustomerGroup(String customerGroup);
}
