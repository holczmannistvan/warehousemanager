package com.thesis.warehousemanager.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thesis.warehousemanager.model.Supplier;

@Transactional
@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Integer> {
	Supplier findByName(String name);
	Collection<Supplier> findByNameContains(String suppplierName);
}
