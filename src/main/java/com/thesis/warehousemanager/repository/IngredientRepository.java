package com.thesis.warehousemanager.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.thesis.warehousemanager.model.Ingredient;

@Transactional
@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, Integer>{
	Ingredient findByName(String name);
	Collection<Ingredient> findByNameContains(String name);
}
