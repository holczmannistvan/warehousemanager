
--
-- Initial data for table `supplier`
--

INSERT INTO `supplier` VALUES (1,'38520000023237',1,'1119 Budapest, Szent László utca 3.','BioBolt'),(2,'6011111111111117',1,'1025 Budapest, Vízafogó utca 124.' ,'The Body Shop'),(3,'2223520043560014',1,'2485 Szombathely, Rákóczi utca 5.','Abacus Kft.'),(4,'4003830171874018',1,'4856 Kecskemét, Alabástrom utca 3.','Szappant főzök Kft.'),(5,'3566002020360505',1,'4569 Győr, Fakopáncs utca 2.','Kézműhely Kft.');


INSERT INTO `ingredient` VALUES (1,1,2753,'Olive oil',250,1),(2,1,1035,'Lye',600,5),(3,1,2272,'Cocoa butter',20,3),(4,1,2687,'Castor oil',4,2),(5,1,1046,'Coconut oil',20,2),(6,1,620,'Shea butter',600,1),(7,1,2231,'Distilled water',4000,1),(8,1,1960,'Palm oil',2,5),(9,1,335,'Lemongrass essential oil',0,3),(10,1,1652,'Sweet Almond oil',30,4),(11,1,2657,'Grapefruit Seed Extract',10,2),(12,1,1780,'Rosemary essential oil',30,3),(13,1,1289,'Cornstarch',40,5),(14,1,1927,'Eucalyptus essential oil',6,1),(15,1,1531,'Bergamot essential oil',7,2),(16,1,1502,'Safflower seed oil',6,3),(17,1,374,'Lavender oil',3,3),(18,1,2640,'Rosemary leaf',20,3),(19,1,2493,'Rosemary extract',21,1),(20,1,2411,'Sunflower oil',300,5),(21,1,1435,'Peppermint leaf',30,4),(22,1,2535,'Barley grass',20,3),(23,1,759,'Pink grapefruit essential oil',0,2),(24,1,1541,'Honey',4,3),(25,1,1638,'Oats',30,1),(26,1,2239,'Patchouli essential oil',3,1),(27,1,1906,'Glycerin',74,5),(28,1,2455,'Orange essential oil',5,2),(29,1,947,'Annatto seed',12,3),(30,1,2055,'Parsley',4,3);

INSERT INTO `recipe` VALUES (1,1,12825,'Citrus Lavender'),(2,1,16618,'Blood Orange Bergamot'),(3,1,13819,'Oatmeal Spice'),(4,1,19864,'Peppermint Leaf'),(5,1,14733,'Shea Honey Oatmeal'),(6,1,15842,'Pink Grapefruit'),(7,1,16386,'Patchouli'),(8,1,14933,'Gardener’s Hand Soap'),(9,1,23931,'Forest Tonic'),(10,1,12982,'Lavender');

INSERT INTO `recipe_ingredient` VALUES (1,1),(1,2),(1,7),(1,9),(1,12),(1,17),(1,20),(1,27),(2,1),(2,3),(2,7),(2,8),(2,11),(2,15),(2,23),(2,28),(3,1),(3,5),(3,7),(3,8),(3,12),(3,20),(3,25),(4,1),(4,2),(4,4),(4,7),(4,18),(4,19),(4,21),(4,22),(4,30),(5,1),(5,5),(5,6),(5,7),(5,19),(5,20),(5,24),(5,25),(6,2),(6,3),(6,4),(6,7),(6,8),(6,9),(6,11),(6,23),(6,27),(7,1),(7,2),(7,5),(7,7),(7,8),(7,13),(7,14),(7,26),(7,27),(8,1),(8,2),(8,4),(8,7),(8,10),(8,16),(8,21),(8,25),(9,1),(9,3),(9,5),(9,7),(9,9),(9,10),(9,12),(9,14),(9,15),(9,17),(9,19),(9,22),(9,29),(9,30),(10,1),(10,2),(10,6),(10,7),(10,10),(10,17),(10,20),(10,27);

INSERT INTO `product` VALUES (1,1,'Citrus Lavender',427,4,1),(2,1,'Oatmeal Spice',460,54,3),(3,1,'Blood Orange Bergamot',553,20,2),(4,1,'Peppermint Leaf',662,14,4),(5,1,'Shea Honey Oatmeal',491,2,5),(6,1,'Pink Grapefruit',528,24,6),(7,1,'Patchouli',546,8,7),(8,1,'Gardener’s Hand Soap',497,9,8),(9,1,'Forest Tonic',797,95,9),(10,1,'Lavender',432,80,10);

INSERT INTO `comment` VALUES (1,'We love this lavender dream soap recipe because it is perfect for a relaxing spa day ambiance.',1),(2,'To might be able to make this simple soap with items you already have in your kitchen pantry.',1),(3,'Being nourishing for the skin thanks to the Shea butter',10),(4,'This recipe for a handsome, heavenly-scented cold process soap is ideal for a beginner to tackle.',1),(5,'The key to this recipe is the patience and skill required to create the even layers.',10),(6,'Excellent for exfoliating because they remove dead skin cells, moisturize, and improve blood circulation',1);

INSERT INTO `selling` VALUES (1,'Wholesaler',49526,67,6),(2,'Customer',7156,8,8),(3,'Customer',28512,30,6),(4,'Buyer',44190,45,5),(5,'Customer',26875,27,2),(6,'Customer',83613,84,2),(7,'Wholesaler',34132,53,3),(8,'Customer',57645,75,1),(9,'Wholesaler',30933,45,5),(10,'Customer',28980,35,3),(11,'Customer',37065,39,6),(12,'Customer',51645,36,9),(13,'Wholesaler',60536,94,3),(14,'Wholesaler',644,1,3),(15,'Buyer',55200,60,3),(16,'Buyer',103964,94,2),(17,'Buyer',26544,24,2),(18,'Customer',39657,51,10),(19,'Wholesaler',33476,56,1),(20,'Customer',35380,36,7),(21,'Wholesaler',5913,8,6),(22,'Buyer',12766,13,5),(23,'Wholesaler',62680,82,7),(24,'Buyer',75348,69,7),(25,'Buyer',57024,66,10),(26,'Wholesaler',88972,96,4),(27,'Customer',47653,62,1),(28,'Buyer',5460,5,7),(29,'Wholesaler',47308,64,6),(30,'Wholesaler',3822,5,7),(31,'Wholesaler',35481,48,6),(32,'Wholesaler',72290,78,4),(33,'Wholesaler',29223,42,8),(34,'Wholesaler',36432,53,5),(35,'Buyer',72680,79,3),(36,'Customer',32659,42,10),(37,'Buyer',64416,61,6),(38,'Wholesaler',46340,50,4),(39,'Customer',75427,97,10),(40,'Buyer',51008,32,9),(41,'Wholesaler',22243,24,4),(42,'Wholesaler',39928,62,3),(43,'Wholesaler',83412,90,4),(44,'Customer',106160,74,9),(45,'Buyer',46452,42,2),(46,'Buyer',23912,28,1),(47,'Buyer',78526,79,8),(48,'Buyer',10560,10,6),(49,'Buyer',17934,21,1),(50,'Customer',36124,47,1);

INSERT INTO `user` VALUES (1,'admin','ADMIN','admin'),(2,'user','USER','user'),(3,'jbzdfxsb123','USER','ironman'),(4,'njg78xyerg','USER','marvel'),(5,'sjknhre6uy3bcr','USER','thor');





























